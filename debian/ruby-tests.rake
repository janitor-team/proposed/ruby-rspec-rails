require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = FileList['./spec/rspec/**/*_spec.rb'] -
    FileList['./spec/rspec/rails/example/mailer_example_group_spec.rb']
end
